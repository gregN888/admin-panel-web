@extends('admin.dashboard.layouts.app')
@section('content')
    <div class="container1 table">
         <div class="button__div">
          <a href="{{route('dashboard')}}" class="buttonClass"><span>Account</span></a>
          <a href="{{route('properties')}}" class="buttonClass {{$classname}}"><span>Properties</span></a>
          <a href="{{route('images')}}" class="buttonClass"><span>Images</span></a>
         </div>
         <div class="search__container">
            <div>
             <form action="{{route('searchproperties')}}" method="post">
               @csrf
              <input type="text" name="searchtext" placeholder="Search">
              <select name="searchField" id="cars">
              <option value="volvo">Select</option>
                <option value="email">Email</option>
                <option value="address">Address</option>
                <option value="user">User ID</option>
                <option value="Property_id">Property ID</option>
                <option value="status">Status</option>
                {{-- <option value="purchase_date">Date Added</option> --}}
              </select>
              <button type="submit" class="search__button">Search</button>
             </form>
            </div>
  
            <div class="export__element">
              @if ($url==="properties")
                <a class="pagination__element {{ ($data->currentPage() == 1) ? ' hideelement' : '' }}" href="/{{$url}}{{ $data->url($data->currentPage()-1)  }}"><</a>
                <a class="pagination__element {{ ($data->currentPage() == $data->lastPage()) ? ' hideelement' : '' }}" href="/{{$url}}{{ $data->url($data->currentPage()+1) }}">></a>
                @endif
                <form action="{{route('propertiesExport')}}" method="post">
                  @csrf
                 <input type="hidden" name="text" value="@isset($text){{ $text }}@endisset">
                 <input type="hidden" name="dropdown" value="@isset($dropdown){{ $dropdown }}@endisset">
                 <button class="search__button export_button" type="submit">Export</button>
               </form>
            </div>
             
           </div>
 
         <div class="card">
       
          <!-- /.card-header -->
          <div class="card-body table-responsive p-0">
            <table class="table table-hover text-nowrap">
              <thead>
                <tr>
                  <th>User ID</th>
                  <th>Email</th>
                  <th>Property ID</th>
                  <th>Address</th>
                  <th>Likes</th>
                  <th>Date Added</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($data as $Pkey=> $item)
                <tr>
                  <td>{{$item['user']}}</td>
                  <td>{{$item['email']}}</td>
                  <td>{{$item['Property_id']}} </td>
                  <td>{{$item['address']}}</td>
                  
                  <td>
                      @php
                      if(array_key_exists("likes",$item)){
                        echo $item["likes"];
                      }
                      @endphp
                  </td>
                  <td>
                   <script>
                    var adddate={{$item['purchase_date']}}
                    var date = new Date((adddate));//data[k].timestamp
                    document.write(date.toLocaleDateString());
                   </script>
                  @php
                      echo $item['purchase_date'];
                      // echo gettype($item['purchase_date']);
                  @endphp    
                </td>
                  <td id="property-{{$item['Property_id']}}">@php
                      if (array_key_exists("deleted",$item)){
                        foreach ($item as $key => $value) {
                             if($key==="deleted"){
                                 if($item[$key]){
                                  echo "Deleted";
                                 } 
                                 else {
                                  echo "Active";
                                 }
                             }
                      }
                      }
                      else {
                          echo 'Active';
                      }
                      
                  @endphp</td>
                  <td>
                  <button class="btn btn-link" id="propertybutton-{{$item['Property_id']}}" onclick="actionbutton('{{$item['Property_id']}}')">
                    @php
                      if (array_key_exists("deleted",$item)){
                        foreach ($item as $key => $value) {
                             if($key==="deleted"){
                                 if($item[$key]){
                                    echo 'Reverse';      
                                 } 
                                 else {
                                  echo "Delete";
                                 }
                             }
                      }
                      }
                      else {
                          echo 'Delete';
                      }
                  @endphp
                  </button>
                  </td>
                </tr>
                @endforeach
             
               
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
          </div>
    </div>
@endsection

@push('page_scripts')
<script>

    const addClass=document.querySelectorAll('.buttonClass');
    const loadercontainer=document.getElementById('loader_container');
    for (const elemnet of addClass) {
        elemnet.addEventListener('click',(e)=>{
            e.target.classList.add('active')
        })
    }
    const actionbutton=(id)=>{
      loadercontainer.classList.add('active');
      const statusdocument=document.getElementById(`property-${id}`);
      const statusButton=document.getElementById(`propertybutton-${id}`);
      
      const xhr=new XMLHttpRequest();
      const url=`/updateproperties/${id}`;
      xhr.open('GET',url,true);
      xhr.onload=function(){
        const myresponse=JSON.parse(this.responseText);
          if(myresponse.status=="success"){
             if(statusdocument.innerText=="Deleted"){
                 statusdocument.innerText="Active";
                 statusButton.innerText="Delete";
             }
              else{
                 statusdocument.innerText="Deleted";
                 statusButton.innerText="Reverse";
              }
              loadercontainer.classList.remove('active');
            }
          }
      xhr.send();
    };
</script>

@endpush