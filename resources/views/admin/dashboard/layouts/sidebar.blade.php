<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
    
        <li class="nav-item">
            <a href="" class="nav-link">
                <i class="nav-icon fas fa-th"></i>
                <p>
                DashBoard          
                </p>
            </a>
        </li>
        
      <li class="nav-item">
        <a href="#" class="nav-link">
            <i class="nav-icon fas fa-gift"></i>
          <p>
            Users
            <i class="fas fa-angle-left right"></i>
          </p>
        </a>
        <ul class="nav nav-treeview">
        
          <li class="nav-item">
            <a href="" class="nav-link">
                <i class="nav-icon far fa-eye"></i>
              <p>Users List</p>
            </a>
          </li>
        </ul>
      </li>
      <li class="nav-header">User Action</li>
      <li class="nav-item" style="margin-left: 20px;margin-top:10px">
        <i class="nav-icon far fa-circle text-danger"></i>
        <a href="#"
        onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
        Logout
        </a>
        <form id="logout-form" action="" method="POST" class="d-none">
            @csrf
        </form> 
       
      </li>
    </ul>
  </nav>