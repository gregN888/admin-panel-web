@extends('admin.dashboard.layouts.app')
@section('content')
    <div class="container1 table">
         <div class="button__div">
          <a href="{{route('dashboard')}}" class="buttonClass"><span>Account</span></a>
          <a href="{{route('properties')}}" class="buttonClass"><span>Properties</span></a>
          <a href="{{route('images')}}" class="buttonClass {{$classname}}"><span>Images</span></a>
         </div>
         <div class="search__container">
            <div>
             <form action="{{route('searchimage')}}" method="post">
               @csrf
              <input type="text" name="searchtext" placeholder="Search">
              <select name="searchField" id="cars">
              <option value="volvo">Select</option>
                <option value="email">Email</option>
                <option value="user_id">User ID</option>
                <option value="image_index">Image ID</option>
                <option value="location">Location</option>
                <option value="properties_id">Property ID</option>
              </select>
              <button type="submit" class="search__button">Search</button>
             </form>
            </div>
  
            <div class="export__element">
              @if ($url==="images")
                <a class="pagination__element {{ ($data->currentPage() == 1) ? ' hideelement' : '' }}" href="/{{$url}}{{ $data->url($data->currentPage()-1)  }}"><</a>
                <a class="pagination__element {{ ($data->currentPage() == $data->lastPage()) ? ' hideelement' : '' }}" href="/{{$url}}{{ $data->url($data->currentPage()+1) }}">></a>
                @endif
                <form action="{{route('imagesExport')}}" method="post">
                  @csrf
                 <input type="hidden" name="text" value="@isset($text){{ $text }}@endisset">
                 <input type="hidden" name="dropdown" value="@isset($dropdown){{ $dropdown }}@endisset">
                 <button class="search__button export_button" type="submit">Export</button>
               </form>
            </div>
             
           </div>
 
         <div class="card">
       
          <!-- /.card-header -->
          <div class="card-body table-responsive p-0">
            <table class="table table-hover text-nowrap">
              <thead>
                <tr>
                  <th>User ID</th>
                  <th>Email</th>
                  <th>Image Id</th>
                  <th>Location</th>
                  <th>Property ID</th>
                  <th>Month</th>
                  <th>Year</th>
                  <th>Date Added</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($data as $Pkey=> $item)
                @if (array_key_exists("image",$item))
                <tr>
                  <td>
                    @php
                        if(array_key_exists("user_id",$item)){
                           echo $item["user_id"];
                        }
                    @endphp
                  </td>
                  <td>
                    @php
                    if(array_key_exists("email",$item)){
                       echo $item["email"];
                    }
                     @endphp
                    </td>
                  <td>
                    @if (array_key_exists("image_index",$item))
                        @if (array_key_exists("image",$item))
                            @if (strlen($item["image"])>0)
                            <a href="{{$item["image"]}}">{{$item["image_index"]}}</a>  
                            @else
                            <p>{{$item["image_index"]}}</p>
                            @endif    
                        @else
                            <p>{{$item["image_index"]}}</p>
                        @endif
                    @endif
                    
                    </td>
                  <td>
                    @php
                    if(array_key_exists("location",$item)){
                       echo $item["location"];
                    }
                     @endphp
                  </td>
                  
                  <td>
                    @php
                    if(array_key_exists("properties_id",$item)){
                       echo $item["properties_id"];
                    }
                     @endphp
                  </td>
                  <td>
                    @php
                    if(array_key_exists("month",$item)){
                       echo $item["month"];
                    }
                     @endphp
                </td>
                <td>
                  @php
                    if(array_key_exists("year",$item)){
                       echo $item["year"];
                    }
                  @endphp
                </td>
                <td>
                 @if (array_key_exists('added_date',$item))
                 <script>
                  var adddate={{$item['added_date']}}
                  var date = new Date((adddate));//data[k].timestamp
                 document.write(date.toLocaleString());
                </script>
                 @endif
                </td>
                  <td>
                    <span id="status-{{$item['properties_id']}}-{{$item['image_index']}}-{{$item['reserve']}}">
                      @if ($item["reserve"]==1)
                        Deleted  
                      @else
                        Active
                      @endif
                
                    </span> 
                    
                  </td>
                  <td>
                  <div id="imagebutton-{{$item['properties_id']}}">
                    <button class="btn btn-link"id="actionbutton-{{$item['properties_id']}}-{{$item['image_index']}}-{{$item['reserve']}}" onclick="handleaction('{{$item['properties_id']}}','{{$item['image_index']}}','{{$item['reserve']}}')" >
                      @if ($item["reserve"]==1)
                      Reverse
                      @else
                      Delete
                      @endif
                      </button>
                    {{-- @if (array_key_exists('reserve',$item))
                       <button class="btn btn-link"id="reverse-{{$item['properties_id']}}" onclick="handleareverse('{{$item['properties_id']}}','{{$item['image_index']}}')" >Resverse</button>
                    @else
                       <button class="btn btn-link" id="delete-{{$item['properties_id']}}" onclick="handledelete('{{$item['properties_id']}}','{{$item['image_index']}}')">Delete</button>
                    @endif --}}
                  </div>
                  </td>
                </tr>
                @endif
                @endforeach
             
               
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        

      

      </div>
    
@endsection

@push('page_scripts')
<script>
    const loadercontainer=document.getElementById('loader_container');
    const addClass=document.querySelectorAll('.buttonClass')
    for (const elemnet of addClass) {
        elemnet.addEventListener('click',(e)=>{
            e.target.classList.add('active')
        })
    }
    const handleaction=(id,imgindex,reverse)=>{
      loadercontainer.classList.add('active');
      console.log(id,imgindex,reverse);
      const actionButton=document.getElementById(`actionbutton-${id}-${imgindex}-${reverse}`);
      const statusaction=document.getElementById(`status-${id}-${imgindex}-${reverse}`);
      console.log(actionButton.innerText,statusaction.innerText)
      if(actionButton.innerText=="Delete"){
         const xhr=new XMLHttpRequest();
         const url=`/deleteimage/${id}?image=${imgindex}`;
         xhr.open('GET',url,true);
         xhr.onload=function(){
         const response=JSON.parse(this.responseText);
         if(response.status==="success"){
          actionButton.innerText="Reverse";
            statusaction.innerText="Deleted";
            loadercontainer.classList.remove('active');
          }
         }
         xhr.send(); 

        
      }
      else{
        const xhr=new XMLHttpRequest();
        const url=`/reverseImage/${id}?image=${imgindex}`;
        xhr.open('GET',url,true);
        xhr.onload=function(){
          const response=JSON.parse(this.responseText);
          if(response.status=="success"){
          actionButton.innerText="Delete";
            statusaction.innerText="Active";
            loadercontainer.classList.remove('active');
          }
         }
        xhr.send();
        
      }
     
    };

  

</script>

@endpush