@extends('admin.dashboard.layouts.app')
@section('content')
    <div class="container1 table">
         <div class="button__div">
          <a href="{{route('dashboard')}}" class="buttonClass {{$classname}}"><span>Account</span></a>
          <a href="{{route('properties')}}" class="buttonClass"><span>Properties</span></a>
          <a href="{{route('images')}}" class="buttonClass"><span>Images</span></a>
         </div>
         <div class="search__container">
          <div>
           <form action="{{route('searchaccount')}}" method="post">
             @csrf
            <input type="text" name="searchtext" placeholder="Search" value="@isset($text){{ $text }}@endisset">
            <select name="searchField" id="cars">
            <option value="@isset($dropdown){{ $dropdown }}@endisset">Select</option>
            <option value="name">Name</option>
            <option value="email">Email</option>
            <option value="platform">PlatForm</option>
            <option value="User_id">User ID</option>
            </select>
            <button type="submit" class="search__button">Search</button>
           </form>
          </div>

          <div class="export__element">
              @if ($url==="dashboard")
              <a class="pagination__element {{ ($data->currentPage() == 1) ? ' hideelement' : '' }}" href="/{{$url}}{{ $data->url($data->currentPage()-1)  }}"><</a>
              <a class="pagination__element {{ ($data->currentPage() == $data->lastPage()) ? ' hideelement' : '' }}" href="/{{$url}}{{ $data->url($data->currentPage()+1) }}">></a>
              @endif
              <form action="{{route('accountExport')}}" method="post">
                 @csrf
                <input type="hidden" name="text" value="@isset($text){{ $text }}@endisset">
                <input type="hidden" name="dropdown" value="@isset($dropdown){{ $dropdown }}@endisset">
                <button class="search__button export_button" type="submit">Export</button>
              </form>
          </div>
           
         </div>
 
         {{-- <div class="card">
       
          <!-- /.card-header -->
          <div class="card-body table-responsive p-0">
            <table class="table table-hover text-nowrap">
              <thead>
                <tr>
                  <th>User ID</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Platform</th>
                  <th>Property Count</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($data as $Pkey=> $item)
                <tr>
                  <td> @php
                      if(array_key_exists("User_id",$item)){
                        echo $item["User_id"];
                      }
                      else {
                        echo $Pkey;
                      }
                  @endphp</td>
                  <td>  @php
                    if (array_key_exists("email",$item)){
                      echo $item['name'];
                      }
                 @endphp</td>
                  <td>
                    @php
                    if (array_key_exists("email",$item)){
                        echo $item['email'];
                      }
                 @endphp
                 </td>
                  <td>@php
                      if (array_key_exists("platform",$item)){
                        echo $item['platform'];
                      }
                  @endphp</td>
                  <td>@php
                      if (array_key_exists("properties",$item)){
                        echo $item['properties'];
                      }
                  @endphp</td>
                  <td>@php
                    if (array_key_exists("notif",$item)){
                      foreach ($item as $key => $value) {
                           if($key==="notif"){
                               if($item[$key]){
                                echo "Deleted";
                               } 
                               else {
                                echo "Active";
                               }
                           }
                    }
                    }
                    else {
                        echo 'Active';
                    }
                    
                @endphp</td>
                  <td> <a href="{{route('updateuser',$item['User_id'])}}">
                    @php
                      if (array_key_exists("notif",$item)){
                        foreach ($item as $key => $value) {
                             if($key==="notif"){
                                 if($item[$key]){
                                    echo 'Reverse'; 
                                 } 
                                 else {
                                    echo "Delete";
                                 }
                             }
                      }
                      }
                      else {
                          echo 'Delete';
                      }
                  @endphp
                  </a></td>
                </tr>
                @endforeach
             
               
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
        </div> --}}



   
          
       
          <div id="mytable" class="dataTables_wrapper dt-bootstrap4">
              <div class="row">
                <div class="col-sm-12">
              <table id="example2" class="table table-bordered table-hover dataTable dtr-inline" role="grid" aria-describedby="example2_info">
              <thead>
                <tr>
                  <th>User ID</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Platform</th>
                  <th>Property Count</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($data as $Pkey=> $item)
                <tr>
                  <td> @php
                      if(array_key_exists("User_id",$item)){
                        echo $item["User_id"];
                      }
                      else {
                        echo $Pkey;
                      }
                  @endphp</td>
                  <td>  @php
                    if (array_key_exists("email",$item)){
                      echo $item['name'];
                      }
                 @endphp</td>
                  <td>
                    @php
                    if (array_key_exists("email",$item)){
                        echo $item['email'];
                      }
                 @endphp
                 </td>
                  <td>@php
                      if (array_key_exists("platform",$item)){
                        echo $item['platform'];
                      }
                  @endphp</td>
                  <td>@php
                      if (array_key_exists("properties",$item)){
                        echo $item['properties'];
                      }
                  @endphp</td>
                  <td id="user-{{$item['User_id']}}">@php
                    if (array_key_exists("notif",$item)){
                      foreach ($item as $key => $value) {
                           if($key==="notif"){
                               if($item[$key]){
                                echo "Deleted";
                               } 
                               else {
                                echo "Active";
                               }
                           }
                    }
                    }
                    else {
                        echo 'Active';
                    }
                    
                @endphp</td>
                  <td> <button class="btn btn-link" id="userbutton-{{$item['User_id']}}" onclick="handleaction('{{$item['User_id']}}')">
                    @php
                      if (array_key_exists("notif",$item)){
                        foreach ($item as $key => $value) {
                             if($key==="notif"){
                                 if($item[$key]){
                                    echo 'Reverse'; 
                                 } 
                                 else {
                                    echo "Delete";
                                 }
                             }
                      }
                      }
                      else {
                          echo 'Delete';
                      }
                  @endphp
                  </button></td>
                </tr>
                @endforeach
             
               
              </tbody>
            
            </table>
          </div>
        </div>
      </div>

       

      
@endsection

@push('page_scripts')
<script>

    const addClass=document.querySelectorAll('.buttonClass');
    const loadercontainer=document.getElementById('loader_container');
    for (const elemnet of addClass) {
        elemnet.addEventListener('click',(e)=>{
            e.target.classList.add('active')
        })
    }

    const handleaction=(id)=>{
      loadercontainer.classList.add('active');
      const statusdocument=document.getElementById(`user-${id}`);
      const statusButton=document.getElementById(`userbutton-${id}`);
      
      const xhr=new XMLHttpRequest();
      const url=`/updateuser/${id}`;
      xhr.open('GET',url,true);
      xhr.onload=function(){
          const myresponse=JSON.parse(this.responseText);
          if(myresponse.status=="success"){
            if(statusdocument.innerText=="Deleted"){
            statusdocument.innerText="Active";
            statusButton.innerText="Delete";
          }
          else{
            statusdocument.innerText="Deleted";
            statusButton.innerText="Reverse";
          }
          loadercontainer.classList.remove('active');
        }
      }
      xhr.send(); 
    }
    
    var date = new Date((1525227766000));//data[k].timestamp
    console.log(date);



</script>

@endpush