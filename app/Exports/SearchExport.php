<?php

namespace App\Exports;
use Maatwebsite\Excel\Concerns\FromCollection;
use App\Search\SearchResult;
use Maatwebsite\Excel\Concerns\WithHeadings;

class SearchExport implements FromCollection,WithHeadings
{
    protected $text;
    protected $dropdown;
    protected $totalRecord;
    protected $heading;
    public function __construct($totalRecord,$heading,$text,$dropdown) {
        $this->text = $text;
        $this->dropdown = $dropdown;
        $this->totalRecord = $totalRecord;
        $this->heading = $heading;
     }
    public function headings():array{
        return $this->heading;
    }
    public function collection()
    {
        $accout_object=new SearchResult("accounts");
        $data=$accout_object->searchAccount($this->totalRecord,$this->text,$this->dropdown);
        return collect($data);
    }
}