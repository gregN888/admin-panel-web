<?php

namespace App\Exports;
use Maatwebsite\Excel\Concerns\FromCollection;
use App\Search\SearchResult;
use Maatwebsite\Excel\Concerns\WithHeadings;

class AccountsExport implements FromCollection,WithHeadings
{
    protected $totalRecord;
    protected $heading;
    public function __construct($totalRecord,$heading) {
        $this->totalRecord = $totalRecord;
        $this->heading = $heading;
    }
    public function headings():array{
        return $this->heading;
    }
    public function collection()
    {
        return collect($this->totalRecord);
    }
}