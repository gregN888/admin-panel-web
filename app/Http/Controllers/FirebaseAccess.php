<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Search\SearchResult;
use App\CusPagination\CustomPaginate;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\AccountsExport;
use App\Exports\SearchExport;

class FirebaseAccess extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }
  public function allHeading(){
    return[
      "Gmail",
      "Name",
      "token",
      "currency",
      "date_created",
      "phone_number",
      "platform",
      "privacy_policy",
      "tos",
      "U_id",
      "properties",
    ];
  }

 public function getrecord(){
    $accout_object=new SearchResult('accounts');
    $final_result=$accout_object->final_result();
    $paginator_obj = new CustomPaginate();
    $data=$paginator_obj->paginate($final_result);
    $classname="active";
    $url="dashboard";
    return view('admin.dashboard.adminPanel',compact('data','classname','url'));
 }   
 
 public function searchResult(Request $request){
   $text=$request->searchtext;
   $dropdown=$request->searchField;
   $accout_object=new SearchResult('accounts');
   $final_result=$accout_object->final_result();
   $data=$accout_object->searchAccount($final_result,$request->searchtext,$request->searchField);
  //  $paginator_obj = new CustomPaginate();
  //  $data=$paginator_obj->paginate($accounts);
   $classname="active";
   $url="searchaaccount";
   return view('admin.dashboard.adminPanel',compact('data','classname','url','text','dropdown'));
 }

    public function export(Request $request){
      
        $accout_object=new SearchResult('accounts');
        $final_result=$accout_object->final_result();
        if (is_null($request->dropdown) || is_null($request->text)) {
          return Excel::download(new AccountsExport($final_result,$this->allHeading()), 'accounts.csv');
        } else {
        
          return Excel::download(new SearchExport($final_result,$this->allHeading(),$request->text,$request->dropdown), 'accounts.csv');
        }

    }

    public function Update($id){
        $update_array=[];
        $user_obj=new SearchResult('users/'.$id);
        $all_result=$user_obj->result();
      
        if(array_key_exists("notif",$all_result)){
            if(!$all_result['notif']){
                $update_array=array_merge($all_result,['notif'=>true,"prop_notif"=>true]);
            }
            else{
                $update_array=array_merge($all_result,['notif'=>false,"prop_notif"=>false]);
            }
        }
          else{
              $update_array=array_merge($all_result,['notif'=>true,"prop_notif"=>true]);
          }
        
      
        $user_obj->updateData($update_array);
        $response = array(
          'status' => 'success',
          'msg' => "succfully update",
      );
      return response()->json($response); 
    }


}
