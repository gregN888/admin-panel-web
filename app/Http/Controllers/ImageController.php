<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Search\ImageSearch;
use App\CusPagination\CustomPaginate;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\AccountsExport;
use App\Exports\SearchExport;



class ImageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function getresult(){
        $img_obj=new ImageSearch("properties");
        return $img_obj->Image_result();
    }

    public function allHeading(){
        return[
          "User Email",
          "User Name",
          "User is Active",
          "Property ID",
          "User ID",
          "image ID",
          "amount",
          "image",
          "late_fee",
          "month",
          "paid",
          "year",
          "location"

        ];
      }

    public function index(){
        $result=$this->getresult();
        // dd($result);
        $paginator_obj = new CustomPaginate();
        $data=$paginator_obj->paginate($result);
        $classname="active";
        $url="images";
        return view('admin.dashboard.imagePanel',compact('data','classname','url'));
    }
    public function search(Request $request){
        $text=$request->searchtext;
        $dropdown=$request->searchField;
        $result=$this->getresult();
        $img_obj=new ImageSearch("properties");
        $data=$img_obj->searchAccount($result,$text,$dropdown);
        // $paginator_obj = new CustomPaginate();
        // $data=$paginator_obj->paginate($search_result);
        $classname="active";
        $url="searchimage";
        return view('admin.dashboard.imagePanel',compact('data','classname','url','text','dropdown'));
    }

    public function export(Request $request){
        $allresult=$this->getresult();
        if (is_null($request->dropdown) || is_null($request->text)) {
         return Excel::download(new AccountsExport($allresult,$this->allHeading()), 'images.csv');
        } else {
         return Excel::download(new SearchExport($allresult,$this->allHeading(),$request->text,$request->dropdown), 'searchimages.csv');
        }
    }
    public function delete(Request $request,$id){
        $properties_obj=new ImageSearch('properties/'.$id);
        $all_result=$properties_obj->result();
        $image=$all_result["units"][0]["rent_rolls"];
        $delete_image=$image[$request->image];
        if(array_key_exists("reserver_image",$all_result)){
            $new_properties=array_merge($all_result,["reserver_image"=>array_merge($all_result["reserver_image"],[$request->image=>$delete_image])]);
        }
        else{
            $new_properties=array_merge($all_result,["reserver_image"=>[$request->image=>$delete_image]]);
        }
        
        $pro_image=[];
        foreach ($new_properties["units"][0]["rent_rolls"] as $key => $item) {
            if($item==null){
                continue;
            }
            if($key!=$request->image){
             $pro_image[]=$item;
            }
        }
        $new_properties["units"][0]["rent_rolls"]=$pro_image;
        $properties_obj->updateData($new_properties);
        $response = array(
            'status' => 'success',
            'msg' => "succfully update",
        );
        return response()->json($response); 
    }
    public function reverse(Request $request,$id){
        $properties_obj=new ImageSearch('properties/'.$id);
        $all_result=$properties_obj->result();
        // dd($all_result);
        $image=$all_result["reserver_image"];
        $reverse_image=$image[$request->image];
        // dd($reverse_image);
        $reser_image=[];
        if(array_key_exists("reserver_image",$all_result)){{
            foreach ($all_result["reserver_image"] as $key => $item) {
                if($item==null){
                    continue;
                }
                if($key!=$request->image){
                 $reser_image[]=$item;
                }
            }
        }
        $all_result["reserver_image"]=$reser_image;
        if(array_key_exists('rent_rolls',$all_result["units"][0])){
            $all_result["units"][0]["rent_rolls"]=array_merge($all_result["units"][0]["rent_rolls"],[$request->image=>$reverse_image]);
        }
        else{
            $all_result["units"][0]=array_merge($all_result["units"][0],["rent_rolls"=>[$request->image=>$reverse_image]]);
        }
        // dd($all_result);
        $properties_obj->updateData($all_result);
        $response = array(
            'status' => 'success',
            'msg' => "succfully update",
        );
        return response()->json($response); 
    }
}
}
