<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Search\PropertiesSearch;
use App\CusPagination\CustomPaginate;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\AccountsExport;
use App\Exports\SearchExport;

class PropertiesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function getresult(){
        $properties_object1=new PropertiesSearch('properties');
        $pro_array=$properties_object1->Combine_properties();
        return $pro_array;
    }
   
    public function allHeading(){
        return[
          "Address",
          "Cash_invested",
          "Deleted",
          "key",
          "Likes",
          "Millis",
          "prop_type",
          "purchase_amt",
          "purchase_date",
          "User id",
          "User Email",
          "User Name",
          "User is Active",
          "Prop_notif",
          "Property_id",
        ];
      }
    

    public function get_properties(){
        // dd($this->getresult());
        $paginator_obj = new CustomPaginate();
        $data=$paginator_obj->paginate($this->getresult());
        $classname="active";
        $url="properties";
        return view('admin.dashboard.properties',compact('data','classname','url'));
    }
    public function get_search(Request $request){
        // dd($request->searchtext);
        $text=$request->searchtext;
        $dropdown=$request->searchField;
        if($dropdown=="status"){
            $text=strtolower($text);
        }
        elseif($dropdown=="purchase_date"){
            // $datetime = new DateTime($text);
            $text=strtotime($text);
            $text=strval($text);
            // dd(gettype($text))

           ;
            // dd($text);
        }
        $properties_obj=new PropertiesSearch('properties');
        $all_result=$this->getresult();
        // dd($all_result);
        $data=$properties_obj->searchAccount($all_result,$text,$dropdown);
        // $paginator_obj = new CustomPaginate();
        // $data=$paginator_obj->paginate($search_result);
        $classname="active";
        $url="searchproperties";
        return view('admin.dashboard.properties',compact('data','classname','url','text','dropdown'));
    }

    public function Update($id){
        $update_array=[];
        $properties_obj=new PropertiesSearch('properties/'.$id);
        $all_result=$properties_obj->result();
      
        if(array_key_exists("deleted",$all_result)){
            if(!$all_result['deleted']){
                $update_array=array_merge($all_result,['deleted'=>true]);
            }
            else{
                $update_array=array_merge($all_result,['deleted'=>false]);
            }
        }
        else{
            $update_array=array_merge($all_result,['deleted'=>true]);
        }
        $properties_obj->updateData($update_array);
        // return redirect()->route('properties');
        $response = array(
            'status' => 'success',
            'msg' => "succfully update",
        );
        return response()->json($response); 
    }

    public function export(Request $request){
        $allresult=$this->getresult();
        if (is_null($request->dropdown) || is_null($request->text)) {
         return Excel::download(new AccountsExport($allresult,$this->allHeading()), 'properties.csv');
        } else {
         return Excel::download(new SearchExport($allresult,$this->allHeading(),$request->text,$request->dropdown), 'searchproperties.csv');
        }
    }

 


}
