<?php

namespace App\Search;

use Kreait\Firebase\Factory;
use App\Search\SearchResult;

class PropertiesSearch extends SearchResult
{
    public function Combine_properties(){
        $this->refrence='users';
        $users=$this->result();
        $this->refrence="properties";
        $properties=$this->result();
        $pro_array=array();
        foreach ($properties as $key => $item) {
            // dd(gettype($item["address"]));
            $latest=$item;
            if(array_key_exists("units",$item)){
                unset($latest["units"]);
            }
            foreach ($latest as $key1 => $item1) {
                if($key1==="likes"){
                  $latest=array_merge( $latest,["likes"=>sizeof( $latest[$key1])]);
                }
            }
            $newarray=array_merge($latest,$users[$item['user']]);
            if(array_key_exists("token",$newarray)){
                unset($newarray["token"]);
            }
            $status_array=[];
            if(array_key_exists("deleted",$item)){
                if($item["deleted"]){
                    $status_array=["status"=>"deleted","Property_id"=>$key,"address"=>str_replace("\n"," ",$item["address"])];
                }
                else{
                    $status_array=["status"=>"active","Property_id"=>$key,"address"=>str_replace("\n"," ",$item["address"])];
                }
            }
            else{
                $status_array=["status"=>"active","Property_id"=>$key,"address"=>str_replace("\n"," ",$item["address"])];
            }
            $pro_array[]=array_merge($newarray,$status_array);

        }
        return $pro_array;
     }
 
  
}
