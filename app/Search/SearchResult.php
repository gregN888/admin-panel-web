<?php

namespace App\Search;

use Kreait\Firebase\Factory;

class SearchResult
{
 protected $refrence;
 public function __construct($refrence) {
    $this->refrence = $refrence;
 }
 public function connection(){
    $factory = (new Factory)
    ->withServiceAccount(__DIR__.'/firebaseKey1.json')
    ->withDatabaseUri('https://realestate-6126a.firebaseio.com/');
    $realtimeDatabase = $factory->createDatabase();
    return $realtimeDatabase;
 }
 

 public function result(){
    $realtimeDatabase=$this->connection();
    $reference = $realtimeDatabase->getReference($this->refrence);
    $accounts = $reference->getValue();
    return $accounts;
 }
 
 public function updateData($value){
    $realtimeDatabase=$this->connection();
    $reference = $realtimeDatabase->getReference($this->refrence);
    $reference->update($value);
}

 public function searchAccount($data,$text,$dropDown){
    $search_result=array();
    foreach ($data as $key => $item) {
        foreach ($item as $innerKey => $value) {
            if($innerKey===$dropDown){
                if(strstr( $item[$innerKey], $text )){
                   if(!array_key_exists("U_id",$item)){
                    $search_result[]= array_merge($item,['U_id'=>$key]);
                   }
                   else{
                    $search_result[]=$item;                   }
                } 
            }
        }
    }
    return $search_result;
 }

 public function final_result(){
     $my_array=array();
     $this->refrence="users";
     $users=$this->result();
     $this->refrence="accounts";
     $account=$this->result();
     $this->refrence="properties";
     $properties=$this->result();
     foreach ($users as $key => $item) {
        if (array_key_exists("email",$item))

        {
            $f_acc=$this->searchAccount($account,$item['email'],"email");
            // if(sizeof($f_acc)===0){
            //     continue;
            // }
            $array1=array_merge($item,sizeof($f_acc)===0?[]:$f_acc[0]);
            $arr2=array_merge($array1,["User_id"=>$key]);
            $arr3=array_merge($arr2,["properties"=>sizeof($this->searchAccount($properties,$key,"user"))]);
            $my_array[]=$arr3;
        }
    
     }
     return $my_array;
 }
 

}
