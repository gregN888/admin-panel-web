<?php

namespace App\Search;

use Kreait\Firebase\Factory;
use App\Search\SearchResult;

class ImageSearch extends SearchResult
{

    public function Image_result(){
        $result=[];
        $final_result=[];
        $properties=$this->result();
        $this->refrence="users";
        $users=$this->result();

        foreach ($properties as $key => $item) {
            $user=$users[$item["user"]];
            unset($user["token"]);
            if(array_key_exists("prop_notif",$user)){
                unset($user["prop_notif"]);
            }
            $detail=array_merge($user,["properties_id"=>$key,"user_id"=>$item["user"]]);
            if(array_key_exists("units",$item)){
              if(array_key_exists("rent_rolls",$item["units"][0])){
                $rent_roll=$item["units"][0]["rent_rolls"];

                foreach ($rent_roll as $key1 => $item1) {
                    if($item1!=null){
                        $result[]=array_merge(array_merge($detail,["image_index"=>$key1,"location"=>"rent_rolls","added_date"=>$item["purchase_date"],"reserve"=>0]),$item1);
                    }            
                }
              }
            }
            if(array_key_exists("reserver_image",$item)){
                $reserver_image=$item["reserver_image"];
                foreach ($reserver_image as $key2 => $item2) {
                    if($item2!=null){
                        $result[]=array_merge(array_merge($detail,["image_index"=>$key2,"location"=>"Deleted Image","reserve"=>1,"added_date"=>$item["purchase_date"]]),$item2);
                    }            
                }
            }
        }
       
        // dd($result);
        return $result;
    }
 
    
}
