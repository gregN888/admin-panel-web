<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});



// Route::get('/login', 'FirebaseAuthentication@LoginForm')->name('loginform');
// Route::post('/login', 'FirebaseAuthentication@Login')->name('login');
// Route::get('/logout', 'FirebaseAuthentication@logout')->name('logout');


Route::get('/dashboard','FirebaseAccess@getrecord' )->name('dashboard');
Route::any('/searchaaccount','FirebaseAccess@searchResult' )->name('searchaccount');
Route::post('/accountExport','FirebaseAccess@export' )->name('accountExport');
Route::get('/updateuser/{id}','FirebaseAccess@Update' )->name('updateuser');

Route::get('/properties','PropertiesController@get_properties' )->name('properties');
Route::any('/searchproperties','PropertiesController@get_search' )->name('searchproperties');
Route::get('/updateproperties/{id}','PropertiesController@Update' )->name('updateproperties');
Route::post('/propertiesExport','PropertiesController@export' )->name('propertiesExport');


Route::get('/images','ImageController@index' )->name('images');
Route::post('/searchimage','ImageController@search' )->name('searchimage');
Route::post('/imagesExport','ImageController@export' )->name('imagesExport');
Route::get('/deleteimage/{id}','ImageController@delete' )->name('deleteimage');
Route::get('/reverseImage/{id}','ImageController@reverse' )->name('reverseImage');

Auth::routes();
// ['register' => false]
Route::get('/home', 'HomeController@index')->name('home');
